/// Calls the first closure where the provided Option == Some(T)
///
/// ```rust
/// use select_outcome::select_option;
///
/// fn is_even(value: i32) -> Option<i32> {
///     if value % 2 == 0{
///         Some(a)
///     } else {
///         None
///     }
/// }
///
///
/// fn main() {
///     let mut book_reviews = HashMap::new();
///
///     // Review some books.
///     book_reviews.insert(
///         "Adventures of Huckleberry Finn".to_string(),
///         "My favorite book.".to_string(),
///     );
///     book_reviews.insert(
///         "Grimms' Fairy Tales".to_string(),
///         "Masterpiece.".to_string(),
///     );
///     book_reviews.insert(
///         "Pride and Prejudice".to_string(),
///         "Very enjoyable.".to_string(),
///     );
///     book_reviews.insert(
///         "The Adventures of Sherlock Holmes".to_string(),
///         "Eye lyked it alot.".to_string(),
///     );
///
///     select_option! {
///         book_reviews.get("Moby Dick") => |x: String| { println("found: {}",x); },
///         book_reviews.get("Hamlet") => |x: String| { println("found: {}",x); },
///         book_reviews.get("All Quiet on the Western Front") => |x: String| { println("found: {}",x); },
///         book_reviews.get("The Adventures of Sherlock Holmes") => |x: String| { println("found: {}",x); },
///         book_reviews.get("Dr. Strangelove, or, How I Learned to Stop Worrying and Love the Bomb") => |x: String| { println("found: {}",x); },
///     }
/// }
/// ```
#[macro_export]
macro_rules! select_option {
    ($($opt:expr => $code:expr),+) => {
        $(
        if $opt.is_some() {
            $code($opt.unwrap());
        }
        else
        )+
        { unreachable!() }
    };
}

/// Calls all closures where the provided Option == Some(T)
/// You have to specify the Option type here with a where clauses simlar that you would use with Traits.
/// The type identifier is T.
///
/// ```rust
/// use select_outcome::select_all_options;
///
/// fn is_even(value: i32) -> Option<i32> {
///     if value % 2 == 0{
///         Some(a)
///     } else {
///         None
///     }
/// }
///
///
/// fn main() {
///     let a = is_even(3);
///     let b = is_even(5);
///     let c = is_even(4);
///     let d = is_even(1);
///     let e = is_even(6);
///     option_select_all! {
///         where T: i32 {
///             a => |x| { println("a = {}; a is even",x); },
///             b => |x| { println("b = {}; b is even",x); },
///             c => |x| { println("c = {}; c is even",x); },
///             d => |x| { println("d = {}; d is even",x); },
///             e => |x| { println("e = {}; e is even",x); },
///         }
///     }
/// }
/// ```
#[macro_export]
macro_rules! select_all_options {
    (where T: $type:ty { $($opt:expr => $code:expr),+ }) => {
        let mut valids: Vec<(fn($type), $type)> = Vec::new();
        $(
            if $opt.is_some() {
                let result_tuple: (fn($type), $type) = ($code, $opt.unwrap());
                valids.push(result_tuple);
            }
        )+
        for valid in valids {
            valid.0(valid.1);
        }
    };
}

#[macro_export]
macro_rules! ensure_ok {
    ($($result:expr),+ => { success: $success:expr, failure: $failure:expr }) => {
        $(
        if $result.is_err() {
            $failure();
        }
        else
        )+
        { $success() }
    };
}
